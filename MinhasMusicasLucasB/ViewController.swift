//
//  ViewController.swift
//  MinhasMusicasLucasB
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit

struct Musica {
    let nomeMusica : String
    let nomeAlbum : String
    let nomeCantor : String
    let nomeImgPequena : String
    let nomeImgGrande : String
    
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var listadeMusica : [Musica] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listadeMusica.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"MinhaCelula", for: indexPath) as! MyCell
        let musica = self.listadeMusica[indexPath.row]
        
        cell.Musica.text = musica.nomeMusica
        cell.Album.text = musica.nomeAlbum
        cell.Cantor.text = musica.nomeCantor
        cell.Imagem.image = UIImage(named: musica.nomeImgPequena)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detalhesViewController = segue.destination as! DetalheMusicaViewController
        let indice = sender as! Int
        let musica = self.listadeMusica[indice]
        
        detalhesViewController.nomeImagem = musica.nomeImgGrande
        detalhesViewController.nomeMusica = musica.nomeMusica
        detalhesViewController.nomeAlbum = musica.nomeAlbum
        detalhesViewController.nomeCantor = musica.nomeCantor
        
        
    }
    

    @IBOutlet weak var tableView: UITableView!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        listadeMusica.append(Musica(nomeMusica: "Pontos Cardeais", nomeAlbum: "Álbum Vivo!", nomeCantor: "Alceu Valença", nomeImgPequena: "capa_alceu_pequeno", nomeImgGrande: "capa_alceu_grande"))
        
        listadeMusica.append(Musica(nomeMusica: "Menor Abandonado", nomeAlbum: "Álbum Patota de Cosme", nomeCantor: "Zeca Pagodinho", nomeImgPequena: "capa_zeca_pequeno", nomeImgGrande: "capa_zeca_grande"))
        
        listadeMusica.append(Musica(nomeMusica: "Tiro ao Álvaro", nomeAlbum: "Álbum Adoniran Barbosa e Convidados", nomeCantor: "Adoniran Barbosa", nomeImgPequena: "capa_adoniran_pequeno", nomeImgGrande: "capa_adhoniran_grande"))
        
    }


}

