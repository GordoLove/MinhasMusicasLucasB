//
//  DetalheMusicaViewController.swift
//  MinhasMusicasLucasB
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit

class DetalheMusicaViewController: UIViewController {

    var nomeImagem: String = ""
    var nomeMusica: String = ""
    var nomeAlbum: String = ""
    var nomeCantor: String = ""
    
    @IBOutlet weak var Imagem: UIImageView!
    @IBOutlet weak var Musica: UILabel!
    @IBOutlet weak var Album: UILabel!
    @IBOutlet weak var Cantor: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.Imagem.image = UIImage(named: self.nomeImagem)
        self.Musica.text = self.nomeMusica
        self.Album.text = self.nomeAlbum
        self.Cantor.text = self.nomeCantor

        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
